Playbook qui permet d'installer et de configurer :
- iptable
- fail2ban
- création d'un user
- ajout clé publique et suppression connexion ssh root
- tmux
- ...
